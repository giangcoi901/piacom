//
//  AppDelegate.h
//  PIACOM
//
//  Created by doanthegiang on 4/6/18.
//  Copyright © 2018 doanthegiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

